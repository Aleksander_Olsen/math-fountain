#pragma once

#include <SDL.h>
#include <SDL_image.h>

#include "Globals.h"

class SDLClass {
private:
	SDL_Rect renderQuad;
public:
	SDLClass();
	~SDLClass();
	SDL_Texture* LoadTexture(std::string path, SDL_Renderer* gRenderer);
	void FreeTexture(SDL_Texture* tex);
	void Render(int x, int y, int size, SDL_Color color, SDL_Renderer* gRenderer);
	void Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer);
	void Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer, float angle, SDL_Point* center = nullptr, SDL_RendererFlip flip = SDL_FLIP_NONE);
};

