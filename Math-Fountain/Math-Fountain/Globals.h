#pragma once

#include <gl\glew.h>
#include <gl\glu.h>


#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <vector>
#include <iostream>
#include <memory>
#include <cmath>

#include "SDLClass.h"

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

const int NUM_PART = 1000;
const float GRAVITY = 80.0;
const float SPAWN_RATE = 0.01;

const float MAX_SPEED_X = 200.0;
const float MIN_SPEED_Y = 200.0;
const float MAX_SPEED_Y = 300.0;

const float MIN_LIFE = 4.0;
const float MAX_LIFE = 6.0;

const float MIN_ANG = 60.0;
const float MAX_ANG = 120.0;

const int MIN_SIZE = 8;
const int MAX_SIZE = 16;
