
#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <ctime>

#include "Globals.h"
#include "Particle.h"
#include "Fountain.h"


//The window we'll be rendering to
SDL_Window* gWindow = nullptr;

//The SDL renderer.
SDL_Renderer* gRenderer = nullptr;

//An SDL class for handeling render and texture loading.
std::unique_ptr<SDLClass> SDL_Class;

std::unique_ptr<Fountain> fountain;

//Clock used to calculate delta time.
Uint32 ticks;
Uint32 lastTicks;
float dt;

//The application will quit if this is true.
bool gQuit = false;

//Starts up SDL, creates window, and initializes OpenGL
bool init();

//Per frame update
void update(float _dt);

//Renders quad to the screen
void render();

//Shut down procedure
void close();



bool init() {
	//Initialization flag
	bool success = true;

	SDL_Class = std::make_unique<SDLClass>();
	fountain = std::make_unique<Fountain>();

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		success = false;
	} else {
		//Set texture filtering to linear
		if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
			printf("Warning: Linear texture filtering not enabled!");
		}

		//Create window
		gWindow = SDL_CreateWindow("Assignment 1", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH + 1, SCREEN_HEIGHT + 1, SDL_WINDOW_SHOWN);
		if (gWindow == NULL) {
			printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			success = false;
		} else {

			//Create renderer for window
			gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);
			if (gRenderer == NULL) {
				printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			} else {
				//Initialize renderer color
				SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

				if (SDL_SetRenderDrawBlendMode(gRenderer, SDL_BLENDMODE_BLEND)) {
					printf("Blend mode could not initialize! SDL Error: %s\n", SDL_GetError());
					success = false;
				}

				//Initialize PNG loading
				int imgFlags = IMG_INIT_PNG;
				if (!(IMG_Init(imgFlags) & imgFlags)) {
					printf("SDL_image could not initialize! SDL_mage Error: %s\n", IMG_GetError());
					success = false;
				}
			}
		}
	}

	return success;
}

void events(SDL_Event& e) {
	//Handle events on queue
	while (SDL_PollEvent(&e) != 0) {
		//User requests quit
		if (e.type == SDL_QUIT) {
			gQuit = true;
		}

		if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
			switch (e.key.keysym.sym) {
			case SDLK_q:
			case SDLK_ESCAPE:
				gQuit = true;
				break;
			}
		}

	}
}

void update(float _dt) {
	fountain->update(_dt);
}

void render() {
	//Clear screen
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0x00);
	SDL_RenderClear(gRenderer);

	fountain->render(SDL_Class, gRenderer);

	//Update screen
	SDL_RenderPresent(gRenderer);
}

void close() {

	//Destroy window	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//t SDL subsystems
	SDL_Quit();
}



int main(int argc, char* args[]) {

	srand(time(0));

	//Start up SDL and create window
	if (!init()) {
		printf("Failed to initialize!\n");
	}
	else {
		//Main loop flag
		gQuit = false;

		//Get the current clock time
		ticks = SDL_GetTicks();
		lastTicks = ticks;

		//Event handler
		SDL_Event eventHandler;

		//While application is running
		while (!gQuit) {
			//Handle events
			events(eventHandler);

			//Get ticks and calculate delta time
			ticks = SDL_GetTicks();
			dt = (ticks - lastTicks) / 1000.0;
			lastTicks = ticks;

			//Handle updates
			update(dt);

			//Render quad
			render();
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}