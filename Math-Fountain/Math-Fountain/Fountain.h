#pragma once

#include "Globals.h"
#include "Particle.h"

class Fountain
{
private:
	float spawn;
	std::vector<std::unique_ptr<Particle>> particles;
public:
	Fountain();
	~Fountain();

	void update(float dt);
	void render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer* gRenderer);
};

