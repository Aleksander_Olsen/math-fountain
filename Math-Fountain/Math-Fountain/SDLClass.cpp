#include "SDLClass.h"



SDLClass::SDLClass() {
	renderQuad = { 0, 0, 0, 0 };
}


SDLClass::~SDLClass()
{
}


SDL_Texture* SDLClass::LoadTexture(std::string path, SDL_Renderer* gRenderer) {

	//The final texture
	SDL_Texture* newTexture = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(path.c_str());
	if (loadedSurface == NULL) {
		printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
	}
	else {
		//Color key image
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		//Create texture from surface pixels
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
		if (newTexture == NULL) {
			printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	//Return success
	return newTexture;
}

void SDLClass::FreeTexture(SDL_Texture* tex) {
	if (tex != NULL) {
		SDL_DestroyTexture(tex);
		tex = NULL;
	}
}

void SDLClass::Render(int x, int y, int size, SDL_Color color, SDL_Renderer * gRenderer) {
	SDL_Rect rect = { x - (size / 2), y - (size / 2), size, size };
	SDL_SetRenderDrawColor(gRenderer, color.r, color.g, color.b, color.a);
	SDL_RenderFillRect(gRenderer, &rect);
}


void SDLClass::Render(int x, int y, SDL_Texture* sprite, SDL_Rect clip, SDL_Renderer* gRenderer) {
	//Set rendering space and render to screen
	renderQuad = { x, y, clip.w, clip.h };

	//Render to screen
	SDL_RenderCopy(gRenderer, sprite, &clip, &renderQuad);
}

void SDLClass::Render(int x, int y, SDL_Texture * sprite, SDL_Rect clip, SDL_Renderer * gRenderer, float angle, SDL_Point * center, SDL_RendererFlip flip) {
	//Set rendering space and render to screen
	renderQuad = { x, y, clip.w, clip.h };

	SDL_RenderCopyEx(gRenderer, sprite, &clip, &renderQuad, angle, center, flip);
}
