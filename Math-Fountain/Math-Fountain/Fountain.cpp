#include "Fountain.h"



Fountain::Fountain() {
	particles.push_back(std::make_unique<Particle>());
}


Fountain::~Fountain()
{
}

void Fountain::update(float dt) {
	spawn += dt;
	if (spawn > SPAWN_RATE && particles.size() < NUM_PART) {
		spawn = 0.0;
		particles.push_back(std::make_unique<Particle>());
	}
	
	for (auto &x : particles) {
		x->update(dt);
	}
}

void Fountain::render(std::unique_ptr<SDLClass>& SDL_Class, SDL_Renderer * gRenderer) {
	for (auto &x : particles) {
		x->render(SDL_Class, gRenderer);
	}
}
