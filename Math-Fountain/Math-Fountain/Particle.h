#pragma once

#include "Globals.h"
#include <SDL.h>

class Particle
{
private:
	float startLife;
	float life;
	float size;
	glm::vec2 pos;
	glm::vec2 vel;
	SDL_Color color;
public:
	Particle();
	Particle(int x, int y, float _life, float angle, float speed, float _size);
	~Particle();

	void spawn();
	void update(float dt);
	void render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer* gRenderer);
};

