#include "Particle.h"



Particle::Particle() {
	spawn();
}

Particle::Particle(int x, int y, float _life, float angle, float speed, float _size) {
	pos = { x, y };
	life = _life;
	float angleRad = angle * (M_PI / 180);
	vel.x = speed * glm::cos(angleRad);
	vel.y = -speed * glm::sin(angleRad);
	size = _size;
}


Particle::~Particle() {
	pos = { 0, 0 };
	life = 0;
	vel = { 0, 0 };
	size = 0;
	
}

void Particle::spawn() {
	pos = { SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 + 100.0};
	startLife = MIN_LIFE + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_LIFE - MIN_LIFE)));
	life = startLife;
	float angle = MIN_ANG + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_ANG - MIN_ANG)));
	float angleRad = angle * (M_PI / 180);
	vel.x = static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / MAX_SPEED_X)) * glm::cos(angleRad);
	vel.y = -(MIN_SPEED_Y + static_cast<float> (rand()) / (static_cast<float> (RAND_MAX / (MAX_SPEED_Y - MIN_SPEED_Y)))) * glm::sin(angleRad);
	size = rand() % (MAX_SIZE - MIN_SIZE) + MIN_SIZE;
	color = { 0, 0, 255, 255 };
}

void Particle::update(float dt) {
	life -= dt;

	if (life > 0.0) {
		int age = (life / startLife) * 255;
		color.a = age;
		
		vel.y += GRAVITY * dt;
		pos += vel * dt;
	} else {
		spawn();
	}
}

void Particle::render(std::unique_ptr<SDLClass> &SDL_Class, SDL_Renderer * gRenderer) {
	if (life > 0.0) {
		SDL_Class->Render(pos.x, pos.y, size, color, gRenderer);
	}
}
